package com.homelab.currencyconverter.common;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.annotation.NonNull;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.homelab.currencyconverter.model.ValCurs;
import com.homelab.currencyconverter.model.Valute;

import org.simpleframework.xml.Serializer;
import org.simpleframework.xml.core.Persister;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

public class Utils {

    @NonNull public static String readStream(InputStream inputStream) throws Exception{
        BufferedReader r = new BufferedReader(new InputStreamReader(inputStream, "windows-1251"));
        StringBuilder total = new StringBuilder();
        String line;
        while ((line = r.readLine()) != null) {
            total.append(line).append('\n');
        }
        return total.toString();
    }

    public static void closeIS(InputStream is) {
        try {
            if (is != null) {
                is.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void writeFile(ValCurs valCurs) {

        Serializer serializer = new Persister();
        File file = new File("data/data/com.homelab.currencyconverter/valcurs.xml");

        if (!file.exists())
            try {
                file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }

        try {
            serializer.write(valCurs, file);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static ValCurs readFile() {
        ValCurs valCurs = null;

        Serializer serializer = new Persister();
        File file = new File("data/data/com.homelab.currencyconverter/valcurs.xml");
        if (file.exists())
            try {
                valCurs = serializer.read(ValCurs.class, file);
            } catch (Exception e) {
                e.printStackTrace();
            }

        return valCurs;
    }

    public static boolean isFile() {
        File file = new File("data/data/com.homelab.currencyconverter/valcurs.xml");
        return file.exists();
    }

    public static boolean isNetworkConnected(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo ni = cm.getActiveNetworkInfo();
        return ni != null && ni.isConnectedOrConnecting();
    }

    public static BigDecimal ConvSTRtoBD(String str){
        BigDecimal bigDecimal = null;

        try {
            DecimalFormat decimalFormat = new DecimalFormat();
            decimalFormat.setParseBigDecimal(true);

            bigDecimal = (BigDecimal) decimalFormat.parse(str);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return  bigDecimal;
    }

    public static void addRadioButtons(Context context, List<Valute> valuteList, RadioGroup rg, String valuteName) {
        List<RadioButton> listOfRadioButtons = new ArrayList<>();

        for (Valute valute : valuteList) {
            RadioButton rb = new RadioButton(context);
            RadioGroup.LayoutParams layoutParams = new RadioGroup.LayoutParams(
                    RadioGroup.LayoutParams.MATCH_PARENT, RadioGroup.LayoutParams.WRAP_CONTENT);
            rb.setLayoutParams(layoutParams);
            rb.setText(valute.getName());
            rb.setPadding(12,24,12,24);
            rb.setTextSize(18);
            rg.addView(rb);
            listOfRadioButtons.add(rb);
        }

        for (RadioButton radioButton : listOfRadioButtons)
            if (radioButton.getText().equals(valuteName))
                radioButton.setChecked(true);
    }

    public static void fillViewComponents(Context context, Button btn, ImageView iv, Valute valute) {
        try {
            btn.setText(valute.getCharCode());
            iv.setImageResource(
                    context.getResources().getIdentifier(
                            "ic_" + valute.getCharCode().toLowerCase(),
                            "drawable", context.getPackageName()));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
