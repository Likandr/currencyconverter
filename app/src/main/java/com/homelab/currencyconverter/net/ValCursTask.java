package com.homelab.currencyconverter.net;

import android.os.AsyncTask;
import android.util.Log;

import com.homelab.currencyconverter.BuildConfig;
import com.homelab.currencyconverter.common.Utils;
import com.homelab.currencyconverter.model.ValCurs;
import com.homelab.currencyconverter.ui.ValCursInteractor;

import org.simpleframework.xml.Serializer;
import org.simpleframework.xml.core.Persister;

import java.io.BufferedInputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class ValCursTask extends AsyncTask<Void, Void, Void> {

    private String LOG_TAG = getClass().getSimpleName();

    private static final String mURL = "http://www.cbr.ru/scripts/XML_daily.asp";

    private boolean isHttpOk = false;
    private ValCurs valCurs;
    private ValCursInteractor.OnFinishedListener mListener;

    public void setProgressListener(ValCursInteractor.OnFinishedListener listener) {
        mListener = listener;
    }

    @Override protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);

        if (isHttpOk) mListener.onSuccess(valCurs);
        else mListener.onError();
    }

    @Override protected Void doInBackground(Void... voids) {
        try {
            Serializer serializer = new Persister();
            valCurs = serializer.read(ValCurs.class, downloadUrl());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    private String downloadUrl() throws Exception {
        InputStream is = null;
        String contentAsString = "";

        URL url = new URL(mURL);
        HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();

        try {
            urlConnection.connect();

            int responseCode = urlConnection.getResponseCode();
            if (BuildConfig.DEBUG) Log.d(LOG_TAG, "The response code is: " + responseCode);

            if (responseCode == HttpURLConnection.HTTP_OK) {
                isHttpOk = true;
                is = new BufferedInputStream(urlConnection.getInputStream());
                contentAsString = Utils.readStream(is);
            }
        } finally {
            urlConnection.disconnect();
            Utils.closeIS(is);
        }

        return contentAsString;
    }
}