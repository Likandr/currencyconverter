package com.homelab.currencyconverter.ui;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.RadioButton;

import com.homelab.currencyconverter.R;
import com.homelab.currencyconverter.common.Utils;
import com.homelab.currencyconverter.model.ValCurs;
import com.homelab.currencyconverter.model.Valute;

import java.math.BigDecimal;

class ValCursPresenterImpl implements ValCursInteractor.OnFinishedListener {

    private ValCursView view;
    private ValCursInteractor valCursInteractor;
    private Context mContext;
    private ValCursActivity mActivity;

    ValCurs mValCurs;
    Valute mValFrom;
    Valute mValTo;

    ValCursPresenterImpl(ValCursActivity activity, ValCursView view, Context context) {
        this.mActivity = activity;
        this.mContext = context;
        this.view = view;
        this.valCursInteractor = new ValCursInteractorImpl();
    }

    void link(ValCursActivity act) {
        mActivity = act;
    }

    void unLink() {
        mActivity = null;
    }

    void fillAfterRestore(Bundle sLS) {
        mActivity.btnResult.setText(sLS.getString("btnResult"));
        mActivity.btnResult.setEnabled(sLS.getBoolean("btnResultEnabled"));
        mActivity.btnCalc.setEnabled(sLS.getBoolean("btnCalcEnabled"));
        fillViewComponents(mValFrom, mValTo);
        mActivity.rl.setVisibility(View.VISIBLE);
    }

    private void fillWithNewData() {
        fillViewComponents(mValFrom, mValTo);
        mActivity.rl.setVisibility(View.VISIBLE);
    }

    private void fillViewComponents(Valute valute1, Valute valute2) {
        Utils.fillViewComponents(mContext, mActivity.btnFrom, mActivity.ivFrom, valute1);
        Utils.fillViewComponents(mContext, mActivity.btnTo, mActivity.ivTo, valute2);
    }

    private void fillViewComponents(Valute valute) {
        if (mActivity.isFROM)
            Utils.fillViewComponents(mContext, mActivity.btnFrom, mActivity.ivFrom, valute);
        else Utils.fillViewComponents(mContext, mActivity.btnTo, mActivity.ivTo, valute);
    }

    void onDestroy() {
        view = null;
    }

    void getValData() {
        valCursInteractor.getValData(mContext, this);
    }

    @Override public void onSuccess(ValCurs valCurs) {
        if (valCurs != null) {
            Utils.writeFile(valCurs);
            mValCurs = valCurs;
            fillData();
        } else {
            mActivity.finish();
        }
    }

    @Override public void onError() {
        if (isFile()) showToast(R.string.no_data_file);
        else {
            showToast(R.string.net_error);
            fillData();
        }
    }

    @Override public void onErrorNetAccess() {
        if (isFile()) fillData();
        showToast(R.string.net_not_available);
    }

    private void fillData() {
        if (mValCurs == null) {
            mValCurs = Utils.readFile();
        }
        mValFrom = mValTo = mValCurs.getList().get(0);
        fillWithNewData();
    }

    void oclCopyResultFunc() {
        String str = mActivity.btnResult.getText().toString();
        ClipboardManager clipboard = (ClipboardManager) mContext.getSystemService(Context.CLIPBOARD_SERVICE);
        ClipData clip = ClipData.newPlainText("", str);
        clipboard.setPrimaryClip(clip);

        mActivity.showToast(str + " - скопировано");
    }

    void oclRBChangeOKFunc() {
        int radioButtonID = mActivity.radioGroup.getCheckedRadioButtonId();
        RadioButton radioButton = (RadioButton) mActivity.radioGroup.findViewById(radioButtonID);
        for (Valute valute : mValCurs.getList()) {
            if (valute.getName() == radioButton.getText()) {
                if (mActivity.isFROM)   mValFrom = valute;
                else                    mValTo = valute;

                fillViewComponents(valute);
            }
        }
    }

    void oclCalculateFunc() {
        BigDecimal valueBD = Utils.ConvSTRtoBD(mActivity.etValue.getText().toString());
        BigDecimal fromBD = Utils.ConvSTRtoBD(mValFrom.getValue());
        BigDecimal toBD = Utils.ConvSTRtoBD(mValTo.getValue());

        BigDecimal resDiv = fromBD.divide(toBD, 4, BigDecimal.ROUND_HALF_EVEN);
        BigDecimal resMul = resDiv.multiply(valueBD);

        BigDecimal resOver = resMul.setScale(2, BigDecimal.ROUND_HALF_UP);

        mActivity.showCalcResult(String.valueOf(resOver));
    }

    boolean oklForEtValueFunc(int i, KeyEvent keyEvent) {
        if ((keyEvent.getAction() == KeyEvent.ACTION_DOWN) && (i == KeyEvent.KEYCODE_ENTER)) {
            if (mValFrom != mValTo) oclCalculateFunc();
            return true;
        }
        return false;
    }

    private void showToast(int id) {
        mActivity.showToast(id);
    }

    boolean isFile() {
        return Utils.isFile();
    }
}