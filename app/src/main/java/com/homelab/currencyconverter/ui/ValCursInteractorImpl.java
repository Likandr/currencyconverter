package com.homelab.currencyconverter.ui;

import android.content.Context;

import com.homelab.currencyconverter.common.Utils;
import com.homelab.currencyconverter.net.ValCursTask;

class ValCursInteractorImpl implements ValCursInteractor {

    @Override public void getValData(Context context, OnFinishedListener listener) {
        if (Utils.isNetworkConnected(context)) {
            ValCursTask valCursTask = new ValCursTask();
            valCursTask.setProgressListener(listener);
            valCursTask.execute();
        } else
            listener.onErrorNetAccess();

    }
}
