package com.homelab.currencyconverter.ui;

import android.content.Context;

import com.homelab.currencyconverter.model.ValCurs;

public interface ValCursInteractor {

    interface OnFinishedListener {

        void onSuccess(ValCurs valCurs);

        void onErrorNetAccess();

        void onError();
    }

    void getValData(Context context, OnFinishedListener listener);
}
