package com.homelab.currencyconverter.ui;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.homelab.currencyconverter.R;
import com.homelab.currencyconverter.common.Utils;

public class ValCursActivity extends AppCompatActivity implements ValCursView{

    boolean isFROM;

    ValCursActivity mActivity;
    Context mContext;

    public ValCursPresenterImpl mPresenter;

    public View rl;
    public EditText etValue;
    public Button btnFrom, btnTo, btnCalc, btnResult;
    public ImageView ivFrom, ivTo;
    public RadioGroup radioGroup;

    @Override protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mActivity = this;
        etValue = (EditText) findViewById(R.id.et_value);

        ivFrom = (ImageView) findViewById(R.id.iv_from);
        ivTo = (ImageView) findViewById(R.id.iv_to);
        btnFrom = (Button) findViewById(R.id.btn_from);
        btnTo = (Button) findViewById(R.id.btn_to);

        btnCalc = (Button) findViewById(R.id.btn_calculate);
        btnResult = (Button) findViewById(R.id.btn_result);

        etValue.addTextChangedListener(mTextEditorWatcher);
        etValue.setOnKeyListener(oklForEtValue);

        ivFrom.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View view) {
                showLocationDialog(true);
            }
        });
        btnFrom.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View view) {
                showLocationDialog(true);
            }
        });
        ivTo.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View view) {
                showLocationDialog(false);
            }
        });
        btnTo.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View view) {
                showLocationDialog(false);
            }
        });
        btnCalc.setOnClickListener(oclCalculate);
        btnResult.setOnClickListener(oclCopyResult);

        rl = findViewById(R.id.rl_activity_main);
        rl.setVisibility(View.INVISIBLE);
        btnResult.setEnabled(false);
        btnCalc.setEnabled(false);

        mContext = this;

        mPresenter = (ValCursPresenterImpl) getLastCustomNonConfigurationInstance();
        if (mPresenter == null) {
            mPresenter = new ValCursPresenterImpl(this, this, this);
            mPresenter.getValData();
        } else {
            mPresenter.link(this);
        }
    }

    @Override protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        mPresenter.fillAfterRestore(savedInstanceState);
    }

    @Override protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("btnResult", btnResult.getText().toString());
        outState.putBoolean("btnResultEnabled", btnResult.isEnabled());
        outState.putBoolean("btnCalcEnabled", btnCalc.isEnabled());
    }

    @Override public Object onRetainCustomNonConfigurationInstance() {
        super.onRetainCustomNonConfigurationInstance();
        mPresenter.unLink();
        return mPresenter;
    }

    @Override public void onDestroy() {
        super.onDestroy();
        mPresenter.onDestroy();
    }

    private void showLocationDialog(final boolean b) {
        isFROM = b;

        final AlertDialog.Builder builder = new AlertDialog.Builder(ValCursActivity.this);
        builder.setTitle("Валюта");

        LayoutInflater inflater = LayoutInflater.from(this);
        final View view = inflater.inflate(R.layout.body_valute_dialogs, null);
        radioGroup = (RadioGroup) view.findViewById(R.id.rg_valute);

        String valuteName = (isFROM ? mPresenter.mValFrom : mPresenter.mValTo).getName();
        Utils.addRadioButtons(mContext, mPresenter.mValCurs.getList(), radioGroup, valuteName);

        builder.setView(view);

        String positiveText = getString(android.R.string.ok);
        builder.setPositiveButton(positiveText, oclRBChangeOK);

        String negativeText = getString(android.R.string.cancel);
        builder.setNegativeButton(negativeText,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //
                    }
                });

        AlertDialog dialog = builder.create();
        dialog.show();
    }

    TextWatcher mTextEditorWatcher = new TextWatcher() {
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            //
        }

        public void onTextChanged(CharSequence s, int start, int before, int count) {
            //
        }

        public void afterTextChanged(Editable s) {
            setEnableBtnCalc();
        }
    };

    View.OnClickListener oclCopyResult = new View.OnClickListener() {
        @Override public void onClick(View view) {
            mPresenter.oclCopyResultFunc();
        }
    };

    View.OnClickListener oclCalculate = new View.OnClickListener() {
        @Override public void onClick(View view) {
            mPresenter.oclCalculateFunc();
        }
    };

    View.OnKeyListener oklForEtValue = new View.OnKeyListener() {
        @Override public boolean onKey(View view, int i, KeyEvent keyEvent) {
            return mPresenter.oklForEtValueFunc(i, keyEvent);
        }
    };

    DialogInterface.OnClickListener oclRBChangeOK = new DialogInterface.OnClickListener() {
        @Override public void onClick(DialogInterface dialogInterface, int i) {
            mPresenter.oclRBChangeOKFunc();
        }
    };

    public void showToast(int id) {
        Toast.makeText(mContext, id, Toast.LENGTH_SHORT).show();
        if (mPresenter.mValCurs == null) finish();
    }

    public void showToast(String string) {
        Toast.makeText(mContext, string, Toast.LENGTH_SHORT).show();
    }

    public void setEnableBtnCalc() {
        if (etValue.getText().toString().trim().length() > 0) {
            btnCalc.setEnabled(true);
        } else
            btnCalc.setEnabled(false);
    }

    public void showCalcResult(String string) {
        btnResult.setText(string);
        btnResult.setEnabled(true);
    }
}