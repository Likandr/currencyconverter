package com.homelab.currencyconverter.ui;

interface ValCursView {

    void showToast(int id);

    void showToast(String string);

    void setEnableBtnCalc();

    void showCalcResult(String string);
}
