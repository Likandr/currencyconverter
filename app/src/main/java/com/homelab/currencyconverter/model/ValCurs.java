package com.homelab.currencyconverter.model;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

import java.util.List;

@Root(name="ValCurs")
public class ValCurs {

    @Attribute
    private String Date;

    @Attribute
    private String name;

    @ElementList(type = Valute.class, inline = true)
    private List<Valute> list;

    public void setDate(String date) {
        Date = date;
    }

    public String getDate() {
        return Date;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Valute> getList() {
        return list;
    }

    public void setList(List<Valute> list) {
        this.list = list;
    }
}